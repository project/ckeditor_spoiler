INTRODUCTION
------------
This module enables the Spoiler plugin from CKEditor.com in your WYSIWYG.

INSTALLATION
------------
1. Download the plugin from http://ckeditor.com/addon/spoiler.
2. Move the spoiler director to the root libraries folder (/libraries).
3. Enable CKEditor Spoiler in the Drupal admin.
4. Configure your WYSIWYG toolbar to include the buttons.

REQUIREMENTS
------------
CKEditor Module (Core)
